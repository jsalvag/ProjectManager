<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'project_id',
        'content',
        'state',//0:nuevo, 1:iniciado, 2:completado, 3:rechazado
        'deadline'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deadline'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
