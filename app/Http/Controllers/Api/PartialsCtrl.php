<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class PartialsCtrl extends Controller
{
    public function pages($view)
    {
        if (view()->exists("pages.$view")) {
            Log::info("La vista pages.$view EXISTE");
            return view("pages.$view");
        }

        Log::info("La vista pages.$view NO EXISTE");

        return view("pages.index");
    }
    /*
        public function index()
        {
            return view('pages.index');
        }

        public function projects()
        {
            return view('pages.projects');
        }

        public function construct()
        {
            return view('pages.construct');
        }
    */
}
