<?php

namespace App\Http\Controllers\Api;

use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::withCount([
            'tasks',
            'tasks AS pending_tasks' => function($q){
                $q->where('state', '!=', 2);
            }
        ])->with('tasks', 'notes')->get();
        return response()->json(compact('projects'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|min:2|max:190|unique:projects,name',
            'description'=>'nullable|string|min:5|max:255',
            'state'=>'required|integer|between:0,3',
            'rfi'=>'nullable|string|size:8'
        ]);

        if($request->json()){
            $project = Project::create($request->only([
                'name',
                'description',
                'state',
                'rfi',
            ]));
            return response()->json(compact('project'), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return response()->json(compact('project'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $this->validate($request, [
            'name'=>'required|string|min:2|max:190|unique:projects,name,'.$project->id,
            'description'=>'nullable|string|min:5|max:255',
            'state'=>'required|integer|between:0,3',
            'rfi'=>'nullable|string|size:8'
        ]);

        if($request->json()){
            $project->fill($request->only([
                'name',
                'description',
                'state',
                'rfi',
            ]))->save();
            $project->refresh();
            return response()->json(compact('project'), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $name = $project->name;
        $project->delete();
        return response()->json([
            'message'=>"$name deleted"
        ], 200);
    }
}
