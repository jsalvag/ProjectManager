angular.module('myApp')
    .controller('ProjectCtrl', ['$scope', '$log', '$uibModal', 'ProjectService', 'toaster', function ($scope, $log, $uibModal, ProjectService, toaster) {
        //console.log('ProjectCtrl...');

        hideAll();

        getProjects();

        $scope.animationsEnabled = true;

        $scope.stateOptions = [
            { name:'Nuevo', value:0 },
            { name:'Pendiente', value:1 },
            { name:'Aprobado', value:2 },
            { name:'Cancelado', value:3 }
        ];

        $scope.processProject = {};

        $scope.open = function (project) {
            hideAll();
            $scope.action = {title: 'Abrir Nuevo Proyecto', ok:'Guardar'};
            if(project){
                $scope.action = {title: 'Actualizar Proyecto', ok:'Actualizar'};
                $scope.selectedProject = project;
            }
            $scope.display.form = true;
        };

        $scope.show = function (project) {
            hideAll();
            $scope.action = {title: 'Proyecto ' + project.name, ok:''};
            $scope.selectedProject = project;
            $scope.display.details = true;
        };

        $scope.grafic = function (project) {
            hideAll();
            $scope.action = {title: ' Gráfico del Proyecto ' + project.name, ok:'Volver'};
            $scope.selectedProject = project;
            $scope.display.graphic = true;
            $scope.dates = project.tasks.map(function (t) {
                return Date.parse(t.deadline);
            });
            console.log($scope.dates);
        };

        $scope.submit = function (project) {
            if (project){
                if (project.id){
                    ProjectService.update({id:project.id}, project).$promise.then(function (response) {
                        console.log(response);
                        hideAll();
                        $scope.selectedProject = project;
                        $scope.display.details = true;
                        toaster.success({title: "Projecto actualizado", body:"El projecto: " + project.name + " has sido actualizado con exito"});
                    }, function (response) {
                        console.log(response);
                    });
                }else{
                    let newProject = new ProjectService(project);
                    newProject.$save().$promise.then(function (resp) {
                        console.log('Se guardo', resp);
                    }, function (resp) {
                        console.log('no se guardo', resp);
                    });
                }
            }
        };

        $scope.cancel = function (project) {
            if(project){
                $scope.show(project);
            }
        };

        $scope.addTask = function (project) {};

        function hideAll() {
            $scope.selectedProject = {};
            $scope.display = {
                details:false,
                form:false,
                graphic:false
            };
            $scope.action = {};
        }

        function getProjects() {

            ProjectService.query().$promise.then(function (resp) {
                $scope.projects = resp.projects;
            }, function (resp) {});
        }

        /**
         * Chart
         */
        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Series A', 'Series B'];
        $scope.data = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        $scope.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }]);