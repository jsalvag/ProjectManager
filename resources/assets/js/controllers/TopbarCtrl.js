angular.module('myApp')
    .controller('TopbarCtrl', ["$scope", function ($scope) {
        //console.log('TopbarCtrl...');

        $scope.menu = [
            {name: "Proyectos", url: "projects", icon: "cubes"},
            {name: "Clientes", url: "clients", icon: "user"},
            {name: "Contacto", url: "contact", icon: "phone"}
        ];

        $scope.responsive = {
            collapse: false
        };
    }]);