angular.module('myApp')
    .controller('MainCtrl', ["$scope", "$rootScope", "$location", function ($scope, $rootScope, $location) {
        //console.log('MainCtrl');
        $rootScope.company = {
            name: "Ego Agency",
            address: "Aguirre 540, C1414ASJ CABA",
            phone: "011 5263-2351"
        };

        $rootScope.activeView = function (path) {
            return path === $location.path();
        }
    }]);