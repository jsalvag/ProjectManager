angular.module('myApp')
    .factory('ProjectService', ['$resource', function ($resource) {
        return $resource('/api/project/:id', {
            id: '@id'
        },{
            query: {
                isArray: false,
                method: 'GET'
            },
            update: {
                method:'PUT'
            }
        });
    }]);