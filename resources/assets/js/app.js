
//require('./bootstrap');
require('./angular-route');
require('./angular-animate');
require('./angular-touch');
require('./ui-bootstrap');
require('./ui-bootstrap-tpls');
require('./angular-resource');
require('./toaster');
require('./angular-chart');

angular.module('myApp', ['ngRoute', 'ngResource', 'toaster', 'ngAnimate', 'ui.bootstrap', 'chart.js']);

require('./config/routes');
require('./services/ProjectService');
require('./directives/ProjectList');
require('./directives/ProjectDetail');
require('./directives/ProjectForm');
require('./directives/ProjectGraphic');
require('./controllers/MainCtrl');
require('./controllers/TopbarCtrl');
require('./controllers/AsideCtrl');
require('./controllers/ProjectCtrl');

