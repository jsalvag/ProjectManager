angular.module('myApp')
    .config(["$routeProvider", function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl:"index",
                controller:"MainCtrl"
            })
            .when('/projects', {
                templateUrl:"projects",
                controller:"ProjectCtrl"
            })
            .otherwise({
                templateUrl:"construct"
            });
    }]);