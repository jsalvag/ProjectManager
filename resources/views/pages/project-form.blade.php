<div class="container-fluid">
    <div class="page-header">
        <h2 ng-bind="action.title"></h2>
    </div>
    <form name="form.projectForm" novalidate>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="form-group col-md-6" ng-class="{'has-error':form.projectForm.name.$invalid && !form.projectForm.name.$pristine}">
                        <input type="text"
                               name="name"
                               id="name"
                               ng-model="selectedProject.name"
                               class="form-control"
                               placeholder="Nombre del Projecto"
                               required
                               maxlength="190"
                               minlength="2"
                        >
                        <span ng-show="form.projectForm.name.$invalid && !form.projectForm.name.$pristine" class="help-block">
                            El campo nombre es <b>requerido</b>, y debe tener <b>entre 2 y 190</b> caracteres</span>
                    </div>
                    <div class="form-group col-md-3" ng-class="{'has-error':form.projectForm.rfi.$invalid && !form.projectForm.rfi.$pristine}">
                        <input type="text" name="rfi" id="rfi" ng-model="selectedProject.rfi" class="form-control" placeholder="RFI" required maxlength="8" minlength="8">
                        <span ng-show="form.projectForm.rfi.$invalid && !form.projectForm.rfi.$pristine" class="help-block">
                            el RFI asociado debe ser de <b>8</b> caracteres</span>
                    </div>
                    <div class="form-group col-md-3" ng-class="{'has-error':form.projectForm.state.$invalid && !form.projectForm.state.$pristine}">
                        <select name="state" ng-model="selectedProject.state" class="form-control">
                            <option ng-repeat="state in stateOptions" ng-value="state.value" ng-bind="state.name"></option>
                        </select>
                        <span ng-show="form.projectForm.state.$invalid && !form.projectForm.state.$pristine" class="help-block">
                            el RFI asociado debe ser de <b>8</b> caracteres</span>
                    </div>
                    <div class="form-group col-md-12" ng-class="{'has-error':form.projectForm.description.$invalid && !form.projectForm.description.$pristine}">
                        <textarea name="description"
                                  ng-model="selectedProject.description"
                                  class="form-control"
                                  placeholder="Descripción"
                                  maxlength="255"
                        ></textarea>
                        <span ng-show="form.projectForm.description.$invalid && !form.projectForm.description.$pristine" class="help-block">
                            La descripción del proyecto debe tener <b>hasta 255</b> cracteres</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button class="btn btn-primary" type="button" ng-click="submit(selectedProject)" ng-bind="action.ok"></button>
                        <button class="btn btn-warning" type="button" ng-click="cancel(selectedProject)">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <pre>@{{ selectedProject | json }}</pre>
</div>