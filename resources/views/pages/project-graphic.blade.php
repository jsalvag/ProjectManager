<div class="container-fluid">
    <div class="page-header">
        <h2 ng-bind="action.title"></h2>
    </div>
    <canvas id="line" class="chart chart-line" chart-data="data"
            chart-labels="labels" chart-series="series" chart-options="options"
            chart-dataset-override="datasetOverride" chart-click="onClick">
    </canvas>
</div>