<div class="page-header">
    <h2>Proyectos</h2>
</div>
<div class="row">
    <div class="col-xs-12 col-md-4">
        <div project-list></div>
    </div>
    <div class="col-xs-12 col-md-8">
        <div project-detail ng-show="display.details"></div>
        <div project-form ng-show="display.form"></div>
        <div project-graphic ng-show="display.graphic"></div>
    </div>
</div>
