
<aside>
    <div ng-show="loading"> cargando <i class="fa fa-spin fa-spinner"></i></div>
    <ul class="nav nav-pills nav-stacked" ng-hide="loading">
        <li class="text-center">
            <button class="btn btn-primary" type="button" ng-click="open(null)">
                Agregar Proyecto <i class="fa fa-plus"></i>
            </button>
        </li>
        <li class="divider"><br></li>
        <li>

            <input type="text" ng-model="query.name" class="form-control" placeholder="Buscar">
        </li>
        <li class="divider"><br></li>
        <li role="presentation" ng-repeat="project in projects | filter:query" >
            <a href="javascript: void(0)" ng-click="show(project)" ng-class="{'active':selectedProject.id == project.id}">
                <span class="pull-right">
                </span>
                @{{ project.name }}
            </a>
        </li>
    </ul>
</aside>

@include('partials.project-modal')