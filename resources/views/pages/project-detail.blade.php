<div class="content">
    <div class="row" ng-show="selectedProject.id">
        <div class="col-sm-12 col-md-6">
            <div class="jumbotron">
                <h3 class="text-capitalize">@{{ selectedProject.name }}</h3>
                <span class="label label-info" title="Fecha de creación">
                    <i class="fa fa-clock-o"></i> <i ng-bind="selectedProject.created_at"></i>
                </span>
                <p class="text-center">
                    <span class="btn-group-sm">
                        <button class="btn btn-info btn-xs" type="button" ng-click="open(selectedProject)" title="Editar Proyecto">
                            <i class="fa fa-edit fa-fw"></i>
                        </button>
                        <button class="btn btn-warning btn-xs" type="button" ng-click="grafic(selectedProject)" title="Ver gráfico de Proyecto">
                            <i class="fa fa-line-chart fa-fw"></i>
                        </button>
                    </span>
                </p>
            </div>
            <fieldset>
                <legend>RFI</legend>
                <div class="well text-center text-uppercase" ng-bind="selectedProject.rfi"></div>
            </fieldset>
            <filedset>
                <legend>Descripción</legend>
                <div class="well text-justify" ng-bind="selectedProject.description"></div>
            </filedset>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tareas
                    <span class="pull-right">
                        <button class="btn btn-info btn-xs" title="Agregar tarea" type="button" ng-click="addTask(project)">
                            <i class="fa fa-plus fa-fw"></i>
                        </button>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="well well-sm">
                        <b>Tareas Totales:</b> <span ng-bind="selectedProject.tasks_count" class="pull-right"></span>
                    </div>
                    <div class="well well-sm">
                        <b>Tareas Pendientes:</b> <span ng-bind="selectedProject.pending_tasks_count" class="pull-right"></span>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Notas
                    <span class="pull-right">
                        <button class="btn btn-info btn-xs" title="Nueva Nota" type="button" ng-click="addNote(project)">
                            <i class="fa fa-plus fa-fw"></i>
                        </button>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="well well-sm" ng-repeat="note in selectedProject.notes">
                        <div class="row">
                            <div class="col-xs-4">
                                <div ng-bind="note.user.name"></div>
                                <div><i class="fa fa-clock-o"></i> <span ng-bind="note.created_at"></span></div>
                            </div>
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p ng-bind="note.content" class="text-justify"></p>
                                    </div>
                                    <div class="col-xs-12">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <pre ng-bind="selectedProject | json"></pre>
        </div>
    </div>
</div>