<script type="text/ng-template" id="projecTasktModal.html">
    <div class="modal-header">
        <h3 class="modal-title" id="modal-title">@{{ modal.action }}</h3>
    </div>
    <form name="form.projectForm" novalidate>
        <div class="modal-body" id="modal-body">
            <div class="form-group" ng-class="{'has-error':form.projectForm.content.$invalid && !form.projectForm.content.$pristine}">
                <textarea name="content"
                          id="content"
                          ng-model="project.content"
                          class="form-control"
                          placeholder="Descripción"
                          maxlength="255">
                </textarea>
                <span ng-show="form.projectForm.content.$invalid && !form.projectForm.content.$pristine" class="help-block">
                    El contenido de la tarea debe tener <b>entre 5 y 255</b> cracteres</span>
            </div>
            <div class="form-group" ng-class="{'has-error':form.projectForm.rfi.$invalid && !form.projectForm.rfi.$pristine}">
                <p class="input-group">
                    <input type="text"
                           class="form-control"
                           uib-datepicker-popup="dd/MM/yyyy"
                           ng-model="task.deadline"
                           is-open="deadline_piker.opened"
                           datepicker-options="dateOptions"
                           ng-required="true"
                           close-text="Cerrar"
                           alt-input-formats="altInputFormats" />

                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-click="open1()"><i
                                class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                </p>
                <span ng-show="form.projectForm.rfi.$invalid && !form.projectForm.rfi.$pristine" class="help-block">
                    el RFI asociado debe ser de <b>8</b> caracteres</span>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="submit()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
    </form>
</script>