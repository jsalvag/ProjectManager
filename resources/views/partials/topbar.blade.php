<nav class="navbar navbar-default" ng-controller="TopbarCtrl">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" ng-click="responsive.collapse = !responsive.collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#/!">@{{ company.name }}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="nav-collapse collapse navbar-responsive-collapse" ng-class="{'in':!responsive.collapse}" uib-collapse="responsive.collapse">
            <ul class="nav navbar-nav">
                <li><a href="#!/"><i class="fa fa-home fa-fw"></i> Inicio</a></li>
                <li ng-repeat="item in menu" ng-class="{ 'active': activeView('/' + item.url) }"><a href="#!@{{ item.url }}">
                        <i class="fa fa-@{{ item.icon }} fa-fw"></i> @{{ item.name }}</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
