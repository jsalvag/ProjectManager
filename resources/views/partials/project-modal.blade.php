<script type="text/ng-template" id="projectModal.html">
    <div class="modal-header">
        <h3 class="modal-title" id="modal-title">@{{ modal.action }}</h3>
    </div>
    <form name="form.projectForm" novalidate>
        <div class="modal-body" id="modal-body">
            <div class="form-group" ng-class="{'has-error':form.projectForm.name.$invalid && !form.projectForm.name.$pristine}">
                <input type="text"
                       name="name"
                       id="name"
                       ng-model="project.name"
                       class="form-control"
                       placeholder="Nombre del Projecto"
                       required
                       maxlength="190"
                       minlength="2"
                >
                <span ng-show="form.projectForm.name.$invalid && !form.projectForm.name.$pristine" class="help-block">
                    El campo nombre es <b>requerido</b>, y debe tener <b>entre 2 y 190</b> caracteres</span>
            </div>
            <div class="form-group" ng-class="{'has-error':form.projectForm.description.$invalid && !form.projectForm.description.$pristine}">
                <textarea type="text"
                          name="description"
                          id="description"
                          ng-model="project.description"
                          class="form-control"
                          placeholder="Descripción"
                          maxlength="255"
                ></textarea>
                <span ng-show="form.projectForm.description.$invalid && !form.projectForm.description.$pristine" class="help-block">
                    La descripción del proyecto debe tener <b>hasta 255</b> cracteres</span>
            </div>
            <div class="form-group" ng-class="{'has-error':form.projectForm.rfi.$invalid && !form.projectForm.rfi.$pristine}">
                <input type="text" name="rfi" id="rfi" ng-model="project.rfi" class="form-control" placeholder="RFI" required maxlength="8" minlength="8">
                <span ng-show="form.projectForm.rfi.$invalid && !form.projectForm.rfi.$pristine" class="help-block">
                    el RFI asociado debe ser de <b>8</b> caracteres</span>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="submit()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
    </form>
</script>