<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \App\User::create([
            'name' => 'Jose Salvador Guevara Lunar',
            'email' => 'jsalvag@gmail.com',
            'password' => bcrypt('123123'),
        ]);
        factory(\App\Project::class)->times(10)->create()->each(function ($p) {
            for ($i = 0; $i < rand(5,20); $i++)
            $p->tasks()->save(factory(\App\Task::class)->make());
            $p->notes()->save(factory(\App\Note::class)->make());
        });
    }
}
