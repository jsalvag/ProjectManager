<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Project::class, function (Faker\Generator $faker) {

    return [
        'name'=>$faker->words(rand(3,5), true),
        'description'=>$faker->paragraphs(rand(0,3), true),
        'rfi'=>str_random(8)
    ];
});

$factory->define(App\Task::class, function (Faker\Generator $faker) {

    return [
        //'project_id'=>1,
        'content'=>$faker->paragraphs(rand(3,5), true),
        'state'=>rand(0,2),//0:nuevo, 1:iniciado, 2:completado, 3:rechazado
        'deadline'=>$faker->dateTimeBetween('today', '3 months')
    ];
});

$factory->define(App\Note::class, function (Faker\Generator $faker) {

    return [
        //'project_id'=>1,
        'content'=>$faker->paragraphs(rand(3,5), true),
        'user_id'=>1
    ];
});
