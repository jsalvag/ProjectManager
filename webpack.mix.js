const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/angular/angular.min.js', 'resources/assets/js');
mix.copy('node_modules/angular-animate/angular-animate.js', 'resources/assets/js');
mix.copy('node_modules/angular-touch/angular-touch.js', 'resources/assets/js');
mix.copy('node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js', 'resources/assets/js');
mix.copy('node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js', 'resources/assets/js');
mix.copy('node_modules/angular-route/angular-route.js', 'resources/assets/js');
mix.copy('node_modules/angular-resource/angular-resource.js', 'resources/assets/js');
mix.copy('node_modules/angularjs-toaster/toaster.js', 'resources/assets/js');
mix.copy('node_modules/angular-chart.js/dist/angular-chart.js', 'resources/assets/js');
mix.copy('node_modules/chart.js/dist/Chart.min.js', 'resources/assets/js');
mix.copy('node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css', 'resources/assets/css');
mix.copy('node_modules/angularjs-toaster/toaster.css', 'resources/assets/css');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .js('resources/assets/js/angular.min.js', 'public/js')
    .js('resources/assets/js/Chart.min.js', 'public/js');

mix.styles([
    'resources/assets/css/ui-bootstrap-csp.css',
    'resources/assets/css/toaster.css'
], 'public/css/all.css');
